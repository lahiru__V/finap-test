import React from 'react'
const Navbar = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className='log'>
                    <a className="navbar-brand" href="#" style={{ color: "#00cc00" }} >FINAP TEST</a>
                </div>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">

                    <ul className="navbar-nav">

                        <li className="nav-item">
                            <a className="nav-link" href="/">Students</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/class">Classroom</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/teacher">Teachers</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/subject">Subject</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/allosub">Allocate Subject</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/alloclass">Allocate Classroom</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="/studerepo">Student Detail Report</a>
                        </li>

                    </ul>
                </div>
            </nav>
        </div>
    )
}

export default Navbar
