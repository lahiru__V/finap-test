import React, { useState, useEffect } from 'react'
import Navbar from '../Navbar/Navbar'
import { FormGroup, Row, Col, Card, CardHeader, CardBody, Button, Label, Input, Form } from 'reactstrap';
import "../scss/main.scss"
import axios from 'axios';

const Classroom = () => {

    const [classroomName, setClassroomName] = useState("")
    const [classroomID, setClassroomID] = useState(0)
    const [classrooms, setClassrooms] = useState([])

    const change = (e) => {

        if (e.target.id === 'classroomName') {
            setClassroomName(e.target.value)
        }

    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = { classroomID, classroomName };
        try {
            const response = await axios.post("http://localhost:35905/Class/SaveClass", data);
            window.location.href = '/class';
            console.log(response.data);
        }
        catch (error) {
            console.log(error.message);
        }
    };

    const handleDelete = async () => {
        try {
            const response = await axios.delete("http://localhost:35905/Class/DeleteClass?classID=" + classroomID);
            window.location.href = '/class';

            console.log(response.data);
        }
        catch (error) {
            console.log(error.message);
        }
    }

    const reset = () => {
        setClassroomID(0)
        setClassroomName("")
    }

    const select = (classroom) => {
        setClassroomName(classroom.classroomName)
        setClassroomID(classroom.classroomID)
    }

    const getClass = async () => {
        try {
            const response = await axios.get('http://localhost:35905/Class/GetClasses');
            setClassrooms(response.data.result);
        }
        catch (error) {
            console.log(error);
        }
    }

    useEffect(() => {
        getClass()
    }, [])

    return (
        <div>
            <Navbar />
            <br />
            <br />
            <br />
            <br />
            <Row>
                <Col md="12" sm="12" xs="12">
                    <Card>
                        <CardHeader>
                            Classroom Details
                        </CardHeader>
                        <CardBody id="classroom_id">
                            <Form onSubmit={handleSubmit}>
                                <FormGroup row>
                                    <Col md="6" sm="12" xs="12">
                                        <Row>
                                            <Col md="12" sm="12" xs="12">
                                                <Label>Class Name</Label>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col md="12" sm="12" xs="12">
                                                <Input id="classroomName" name="classroomName" type="text" value={classroomName} onChange={change} required />

                                            </Col>
                                        </Row>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Col className="offset-3" md="3" sm="3" xs="3">
                                        <Button block className="btn btn-success mr-2" type='submit' >Add/Update</Button>
                                    </Col>
                                    <Col md="3" sm="3" xs="3">
                                        <Button block className="btn btn-danger" onClick={handleDelete}>Delete</Button>
                                    </Col>
                                    <Col md="3" sm="3" xs="3">
                                        <Button block className="btn btn-warning" onClick={reset}>Reset</Button>
                                    </Col>
                                </FormGroup>
                            </Form>
                        </CardBody>
                    </Card>

                    <Card>
                        <CardHeader>
                            Existing Classrooms
                        </CardHeader>
                        <CardBody id="class_details_id">
                            <FormGroup row>
                                <Col md="12" sm="12" xs="12">
                                    <div style={{ overflowX: "auto" }}>
                                        <table className="main-table">
                                            <tbody>
                                                <tr style={{ color: "white", backgroundColor: "#333333" }}>
                                                    <th>Classroom Name</th>
                                                </tr>
                                                {classrooms.map((classroom) => (
                                                    <tr onClick={(e) => select(classroom)}>
                                                        <td>{classroom.classroomName}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </table>
                                    </div>
                                </Col>
                            </FormGroup>
                        </CardBody>
                    </Card>
                </Col>
            </Row>
        </div>
    )
}

export default Classroom
